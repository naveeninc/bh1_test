package org.expicient.flowcontrol;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;

public class GenerateFailedInputValidation extends Exception implements Callable {
    private static final long serialVersionUID = 1L;
    private int status = 200;
    private String reason = "reason";
    private String payload = "";

    public GenerateFailedInputValidation() {
    }

    public int getStatus() {
	return status;
    }

    public void setStatus(int status) {
	this.status = status;
    }

    public String getReason() {
	return reason;
    }

    public void setReason(String reason) {
	this.reason = reason;
    }

    public String getPayload() {
	return payload;
    }

    public void setPayload(String payload) {
	this.payload = payload;
    }

    @Override
    public Object onCall(MuleEventContext eventContext) throws Exception {
	MuleMessage message = eventContext.getMessage();
	message.setInvocationProperty("reason",message.getPayload());
	throw this;
    }

}
